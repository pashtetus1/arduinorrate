enum RotorState { CLOSE, FAR };

RotorState CurrentState = FAR;

const int hallSensorPin = A0;
const int bigLightPin = 2;
const int enginePin = 3;
//ограничение сверху при котором вылючается питание
const int StopEngineRotateLimit = 1450; //об/мин 
//ограничение снизу при котором включается питание
const int StartEngineRotate = 500; //об/мин 

const int timeOn = 5000;
const int timeOff = 1000;
