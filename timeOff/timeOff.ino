#include "main.h"


int defValue = 0;


void setup()
{
  // put your setup code here, to run once:
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(bigLightPin, OUTPUT);
  pinMode(enginePin, OUTPUT);
  Serial.begin(9600);
  defValue = analogRead(hallSensorPin);
  Serial.println("def value: " + (String) defValue);
}

unsigned long lastCloseTime = 0;
byte isRotorMustStop = 0;
float rotatespeed = 0;
float deviation = 0;

void loop()
{
  // put your main code here, to run repeatedly:
    delay(timeOn);
    digitalWrite(bigLightPin, LOW);
    digitalWrite(enginePin, LOW);
    delay(timeOff);
    digitalWrite(bigLightPin, HIGH);
    digitalWrite(enginePin, HIGH);
}

unsigned long lastOutput = 0;
void displayData(unsigned long delta)
{
  unsigned long currentTime = millis();
  if (currentTime - lastOutput >= 1000)
  {
    Serial.println("Delta in miliseconds: " + (String)delta);
    Serial.println("Rotate speed: " + (String)rotatespeed);
    Serial.println("Deviation value: " + (String)deviation);
    lastOutput = currentTime;
  }
}
