enum ControlMode { FREQ, TIME };

//настройки пинов, номера цифровых пинов
const int reflectorSensorPin = 2; 
const int leverPin = 3;
const int enginePin = 4;
const int buildInLedPin = LED_BUILTIN; //13 нога заведенная на встроенный светодиод

///Для частотного режима
//ограничение сверху при котором вылючается питание
const int StopEngineRotateLimit = 1450; //об/мин 
//ограничение снизу при котором включается питание
const int StartEngineRotate = 1000; //об/мин 

///Для временного режима
const int timeOn = 700; //мс
const int timeOff = 5000; //мс

///тайминги
const int displayFreq = 1500; //частота вывода данных в мониторе в мс 
const int chatterTime = 500; //временной отступ для фильтрации дребезка на кнопке в мс

//EEPROM адрес
const int EEPROMaddress = 0;
