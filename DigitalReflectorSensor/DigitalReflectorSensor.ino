#include "main.h"
#include <EEPROM.h>

float rotatespeed = 0;
float rotatespeedMinimal = 9999999;
volatile unsigned long lastPeriod = 0;
volatile unsigned long lastSampleMillis = 0;
volatile ControlMode mode = FREQ;
byte isRotorMustStop = 0;
void setup()
{
  // put your setup code here, to run once:
  pinMode(enginePin, OUTPUT);
  pinMode(reflectorSensorPin, INPUT);
  pinMode(leverPin, INPUT);
  pinMode(buildInLedPin, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(reflectorSensorPin), startWhite, RISING);
  attachInterrupt(digitalPinToInterrupt(leverPin), pushButton, RISING);
  
  Serial.begin(9600);
  mode = (ControlMode) EEPROM.read(EEPROMaddress);
  Serial.println("Start mode: " + (String)mode);
  setupBuildInLed();
}

void loop()
{
  unsigned long currentMillis = millis();
  unsigned long localPeriod = 0;
  
  if (currentMillis - lastSampleMillis > lastPeriod)
  {
    localPeriod = currentMillis - lastSampleMillis;
  }
  else
  {
    localPeriod = lastPeriod;
  }
  
  rotatespeed = 60000.0 / localPeriod;
   
  if (rotatespeed < rotatespeedMinimal)
  { 
    rotatespeedMinimal = rotatespeed;
  }
   
  if (mode == FREQ)
  {
     isRotorMustStop = freqMethod();
  }
  else if (mode == TIME)
  {
     isRotorMustStop = timeMethod();
  }
  
  if (isRotorMustStop)
  {
    digitalWrite(enginePin, LOW);
  }
  else
  {
    digitalWrite(enginePin, HIGH);
  }

  displayData(lastPeriod);

}

byte freqMethod()
{
  if (rotatespeed >= StopEngineRotateLimit)
  {
    return 1;
  }
  else if (rotatespeed <= StartEngineRotate)
  {
    return 0;
  };
  
  return isRotorMustStop;
}

byte timeMethod()
{
  static unsigned long lastMillis = 0;
  
  unsigned long currentMillis = millis();
  unsigned long delta = currentMillis - lastMillis;
 
  if (delta < timeOff)
  {
    return 1;
  }
  
  if (delta < timeOn + timeOff )
  {
    return 0;
  }

  lastMillis = currentMillis;
  return 1;  
}

void pushButton()
{
  static unsigned long firstPush = 0;
  if (millis() - firstPush > chatterTime)
  {
    if (mode == FREQ)
    {
      mode = TIME;
    }
    else
    {
      mode = FREQ;
    };
    Serial.println("changeMode");
    firstPush = millis();
    EEPROM.write(EEPROMaddress, mode);
    setupBuildInLed();
  };
}
void setupBuildInLed()
{
  if (mode == FREQ)
  {
    digitalWrite(buildInLedPin, HIGH);;
  }
  else
  {
    digitalWrite(buildInLedPin, LOW);
  };
}
void startWhite()
{
  static unsigned long currentMillis = 0;
  currentMillis = millis();
  lastPeriod = currentMillis - lastSampleMillis;
  lastSampleMillis = currentMillis;
}

void displayData(unsigned long delta)
{
  static unsigned long lastOutput = 0;
  unsigned long currentTime = millis();
  if (currentTime - lastOutput >= displayFreq)
  {
    String modeName = "Frequency";
    if (mode == TIME)
    {
      modeName = "Time";
    };
    Serial.println("Delta in miliseconds: " + (String)delta);
    Serial.println("Rotate speed: " + (String)rotatespeed);
    Serial.println("Minimal rotate speed: " + (String)rotatespeedMinimal);
    Serial.println("Mode: " + modeName);
    
    rotatespeedMinimal = 999999;
    lastOutput = currentTime;
  }
}
