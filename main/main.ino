#include "main.h"


int defValue = 0;


void setup()
{
  // put your setup code here, to run once:
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(bigLightPin, OUTPUT);
  pinMode(enginePin, OUTPUT);
  Serial.begin(9600);
  defValue = 900;
  Serial.println("def value: " + (String) defValue);
}

unsigned long lastCloseTime = 0;
byte isRotorMustStop = 0;
float rotatespeed = 0;
float rotatespeedMinimal = 9999999;
float deviation = 0;
unsigned int lastMillis = 0;
void loop()
{
  unsigned int currentMillis = millis();
  if (currentMillis - lastMillis > 20)
  {
    lastMillis = currentMillis;
    mainCycle();
  }
}



void mainCycle()
{
  // put your main code here, to run repeatedly:
  int analogValue = analogRead(hallSensorPin); // считываем аналоговое значение
  //  int digitalValue = digitalRead(digitalPin); // считываем цифровое значение
  //  Serial.println((String)analogValue);
  
  deviation = abs( analogValue - defValue ) / (float) defValue;
  //  Serial.println((String)deviation);



  unsigned long currentTime = millis();
  unsigned long delta = currentTime - lastCloseTime;
  if (currentTime < lastCloseTime)
  {
    delta = 1.0E+6;
  }

  if (deviation > 0.3 && CurrentState != CLOSE)
  {
    CurrentState = CLOSE;
    lastCloseTime = currentTime;
    rotatespeed = 60000.0 / delta;

    if (rotatespeed >= StopEngineRotateLimit)
    {
      isRotorMustStop = 1;
    }
    else if (rotatespeed <= StartEngineRotate)
    {
      isRotorMustStop = 0;
    }
    if (rotatespeed < rotatespeedMinimal)
    { 
      rotatespeedMinimal = rotatespeed;
    }
  }
  else if (deviation < 0.2 && CurrentState != FAR)
  {
    CurrentState = FAR;
  }

  if (rotatespeed > 0 && delta > 500 && delta / 3 > (60.0 * 1000 / rotatespeed))
  {
    Serial.println("I think engine stop: " + (String)((60.0 * 1000 / rotatespeed)));
    isRotorMustStop = 0;
    rotatespeed = 0;
  }

  if (CurrentState == CLOSE)
  {
    digitalWrite(LED_BUILTIN, HIGH);
  }
  else
  {
    digitalWrite(LED_BUILTIN, LOW);
  };


  if (isRotorMustStop)
  {
    digitalWrite(bigLightPin, LOW);
    digitalWrite(enginePin, LOW);
  }
  else
  {
    digitalWrite(bigLightPin, HIGH);
    digitalWrite(enginePin, HIGH);
  }

  displayData(delta);

}

unsigned long lastOutput = 0;
void displayData(unsigned long delta)
{
  unsigned long currentTime = millis();
  if (currentTime - lastOutput >= 1000)
  {
    Serial.println("Delta in miliseconds: " + (String)delta);
    Serial.println("Rotate speed: " + (String)rotatespeed);
    Serial.println("Minimal rotate speed: " + (String)rotatespeedMinimal);
    Serial.println("Deviation value: " + (String)deviation);
    rotatespeedMinimal = 999999;
    lastOutput = currentTime;
  }
}
