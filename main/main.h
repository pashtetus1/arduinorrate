enum RotorState { CLOSE, FAR };

RotorState CurrentState = FAR;

const int hallSensorPin = A0;
const int bigLightPin = 2;
const int enginePin = 3;
//ограничение сверху при котором вылючается питание
const int StopEngineRotateLimit = 1400; //об/мин 
//ограничение снизу при котором включается питание
const int StartEngineRotate = 1000; //об/мин 
